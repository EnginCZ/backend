# Secure bin

## Popis projektu

Pointa projektu je vytvořit aplikaci pro možnost sdílení textu pomocí vložení, nahrání a přeposlání odkazu druhému uživateli. Na rozdíl od např. Pastebinu bude obsahovat end-to-end šifrování, takže server nebude mít o přenášených datech ponětí a zvýší se tím tak bezpečnost. Šifrování bude komplentě řešeno na frontendu, jako klíč pro šifrování se pouze buď uživatelem zadané heslo, nebo náhodně vygenerovaný klíč při nahrání, na backend se následně zašle pouze objekt `EncryptionData` s atributy `cipherText` a `salt`.

Uživatelé se budou moct zaregistrovat a nahrávat tak privátní text, ty pak budou moci zobrazit pouze vybraní uživatelé. Ti však ale stále budou potřebovat heslo/kód k textu pro jeho odšifrování na frontendu.

## Zprovoznění aplikace

### Mysql

Pro zprovoznení backend aplikace je potřeba nejprve spustit a nakonfigurovat lokální mysql databázi. Možnosti jsou xamp, docker nebo jiné. Poté nastavit uživatelské proměnné `MYSQL_HOST`, `MYSQL_USER` a `MYSQL_PASSWORD` podle nastavení vyší db.

### Aplikace

#### Spuštení v IDE
Aplikace je napsaná v frameworku Spring Boot s využitím nástroje pro správu aplikací Maven. Pro spuštění aplikace je možné si projekt otevřít v IntelliJ IDEA (nebo jiném IDE) a spusit pomocí předdefinované buildovací sekvence.

#### Spuštení v cmd

```
mvn clean package
cd target
java -jar securebin-<PROJECT_VERSION>-SNAPSHOT.jar
```

## Odkazy
[Frontend project](https://gitlab.com/secure-bin/frontend-angular) (work in progress)
