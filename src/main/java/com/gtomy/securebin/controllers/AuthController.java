package com.gtomy.securebin.controllers;

import com.gtomy.securebin.exceptions.HttpErrorException;
import com.gtomy.securebin.models.auth.SecureBinUserDetails;
import com.gtomy.securebin.models.rest.JwtTokenObject;
import com.gtomy.securebin.models.rest.RegisterUser;
import com.gtomy.securebin.models.rest.UserCredentials;
import com.gtomy.securebin.services.UserService;
import com.gtomy.securebin.utils.JwtTokenUtil;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("auth")
public class AuthController {

    @Autowired
    private UserService userService;
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @PostMapping("signin")
    public ResponseEntity<JwtTokenObject> signIn(@RequestBody UserCredentials userCredentials) {
        Authentication authentication;
        try {
            authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(userCredentials.username().toLowerCase(), userCredentials.password()));
        } catch (DisabledException | BadCredentialsException e) {
            return ResponseEntity.badRequest().build();
        }
        final UserDetails userDetails = (SecureBinUserDetails) authentication.getPrincipal();
        final String token = jwtTokenUtil.generateToken(userDetails);
        return ResponseEntity.ok(new JwtTokenObject(token));
    }

    @PostMapping("signup")
    public ResponseEntity signUp(@RequestBody RegisterUser registerUser) {
        if (registerUser.username() == null || registerUser.username().strip().length() < 3) {
            return ResponseEntity.badRequest().body("Username has to have at least 3 characters");
        }
        if (registerUser.password() == null || registerUser.password().strip().length() < 6) {
            return ResponseEntity.badRequest().body("Password has to have at least 6 characters");
        }
        if (registerUser.email() == null || !EmailValidator.getInstance().isValid(registerUser.email())) {
            return ResponseEntity.badRequest().body("Email invalid");
        }
        try {
            userService.registerUser(registerUser.username().toLowerCase(), registerUser.email().toLowerCase(), registerUser.password());
            return ResponseEntity.status(201).build(); // Http status created
        } catch (HttpErrorException e) {
            return ResponseEntity.status(e.STATUS).body(e.getMessage());
        }
    }

}
