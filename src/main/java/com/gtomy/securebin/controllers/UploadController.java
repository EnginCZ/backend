package com.gtomy.securebin.controllers;

import com.gtomy.securebin.exceptions.HttpErrorException;
import com.gtomy.securebin.models.database.SecureBinUser;
import com.gtomy.securebin.models.database.Upload;
import com.gtomy.securebin.models.dtos.SecureBinUserDto;
import com.gtomy.securebin.models.dtos.UploadDto;
import com.gtomy.securebin.models.rest.CreateUpload;
import com.gtomy.securebin.services.UploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("upload")
public class UploadController {

    @Autowired
    private UploadService uploadService;

    public UploadController(UploadService uploadService) {
        this.uploadService = uploadService;
    }

    @GetMapping()
    public ResponseEntity<List<UploadDto>> getPublicUploads(
            @RequestParam int page,
            @RequestParam int size
    ) {
        List<Upload> uploads = uploadService.getPublicUploads(page, size);
        List<UploadDto> uploadDtos = uploads.stream().map(Upload::toUploadDto).toList();
        return ResponseEntity.ok(uploadDtos);
    }

    @GetMapping("{id}")
    public ResponseEntity<UploadDto> getUpload(@PathVariable int id) {
        try {
            return ResponseEntity.ok(uploadService.getUpload(id).toUploadDto());
        } catch (HttpErrorException e) {
            return ResponseEntity.status(e.STATUS).build();
        }
    }

    @GetMapping("users/{id}")
    public ResponseEntity<List<SecureBinUserDto>> getAllowedUsers(
            @PathVariable int id,
            @RequestParam int page,
            @RequestParam int size
    ) {
        try {
            List<SecureBinUser> users = uploadService.getUploadAllowedUsers(id, page, size);
            List<SecureBinUserDto> userDtos = users.stream().map(SecureBinUser::toSecureBinUserDto).toList();
            return ResponseEntity.ok(userDtos);
        } catch (HttpErrorException e) {
            return ResponseEntity.status(e.STATUS).build();
        }
    }

    @PostMapping()
    public ResponseEntity<UploadDto> createUpload(@RequestBody CreateUpload createUpload) {
        if (createUpload.displayName().strip().length() < 1) {
            return ResponseEntity.badRequest().build();
        }
        if (createUpload.encryptionData() == null ||
                createUpload.encryptionData().cipherText().strip().length() < 1 ||
                createUpload.encryptionData().salt().strip().length() < 16
        ) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.status(201).body(uploadService.createUpload(createUpload).toUploadDto()); // Http status created
    }

}
