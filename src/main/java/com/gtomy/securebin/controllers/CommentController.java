package com.gtomy.securebin.controllers;

import com.gtomy.securebin.exceptions.HttpErrorException;
import com.gtomy.securebin.models.database.Comment;
import com.gtomy.securebin.models.dtos.CommentDto;
import com.gtomy.securebin.services.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("comments")
public class CommentController {

    @Autowired
    private CommentService commentService;

    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }

    @PostMapping("{uploadId}")
    public ResponseEntity<CommentDto> postComment(@PathVariable int uploadId, @RequestBody String text) {
        if (text == null || text.strip().length() < 10) {
            return ResponseEntity.badRequest().build();
        }
        try {
            Comment comment = commentService.postComment(uploadId, text);
            return ResponseEntity.ok(comment.toCommentDto());
        } catch (HttpErrorException e) {
            return ResponseEntity.status(e.STATUS).build();
        }
    }

    @DeleteMapping("{commentId}")
    public ResponseEntity deleteComment(@PathVariable int commentId) {
        try {
            commentService.deleteComment(commentId);
            return ResponseEntity.ok().build();
        } catch (HttpErrorException e) {
            return ResponseEntity.status(e.STATUS).build();
        }
    }

    @GetMapping("count/{uploadId}")
    public ResponseEntity<Integer> getNumberOfCommentsOnUpload(@PathVariable int uploadId) {
        try {
            int countOfComments = commentService.getCountOfCommentsUnderUpload(uploadId);
            return ResponseEntity.ok(countOfComments);
        } catch (HttpErrorException e) {
            return ResponseEntity.status(e.STATUS).build();
        }
    }

    @GetMapping("list/{uploadId}")
    public ResponseEntity<List<CommentDto>> getCommentsOnUpload(
            @PathVariable int uploadId,
            @RequestParam int page,
            @RequestParam int size
    ) {
        try {
            List<Comment> comments = commentService.getCommentsUnderUpload(uploadId, page, size);
            List<CommentDto> commentDtos = comments.stream().map(Comment::toCommentDto).toList();
            return ResponseEntity.ok(commentDtos);
        } catch (HttpErrorException e) {
            return ResponseEntity.status(e.STATUS).build();
        }
    }
}
