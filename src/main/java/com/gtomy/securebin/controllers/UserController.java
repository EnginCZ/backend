package com.gtomy.securebin.controllers;

import com.gtomy.securebin.exceptions.HttpErrorException;
import com.gtomy.securebin.exceptions.NotFoundException;
import com.gtomy.securebin.models.database.SecureBinUser;
import com.gtomy.securebin.models.database.SocialLink;
import com.gtomy.securebin.models.database.Upload;
import com.gtomy.securebin.models.dtos.CountOfUploads;
import com.gtomy.securebin.models.dtos.SecureBinUserDto;
import com.gtomy.securebin.models.dtos.SocialLinkDto;
import com.gtomy.securebin.models.dtos.UploadDto;
import com.gtomy.securebin.services.UploadService;
import com.gtomy.securebin.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("user")
public class UserController {

    @Autowired
    private UserService userService;
    @Autowired
    private UploadService uploadService;

    public UserController(UserService userService, UploadService uploadService) {
        this.userService = userService;
        this.uploadService = uploadService;
    }

    @GetMapping()
    public ResponseEntity<SecureBinUserDto> getCurrentUser() {
        Optional<SecureBinUser> user = userService.getCurrentUser();
        if (user.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(user.get().toSecureBinUserDto());
    }

    @GetMapping("uploads/count")
    public ResponseEntity<CountOfUploads> getUploadsCount() {
        try {
            return ResponseEntity.ok(uploadService.getUploadsCount());
        } catch (NotFoundException e) {
            return ResponseEntity.status(e.STATUS).build();
        }
    }

    @GetMapping("uploads/owned")
    public ResponseEntity<List<UploadDto>> getUserUploads(
            @RequestParam int page,
            @RequestParam int size
    ) {
        try {
            List<Upload> uploads = uploadService.getUserUploads(page, size);
            List<UploadDto> uploadDtos = uploads.stream().map(Upload::toUploadDto).toList();
            return ResponseEntity.ok(uploadDtos);
        } catch (NotFoundException e) {
            return ResponseEntity.status(e.STATUS).build();
        }
    }

    @GetMapping("uploads/shared")
    public ResponseEntity<List<UploadDto>> getUserSharedUploads(
            @RequestParam int page,
            @RequestParam int size
    ) {
        try {
            List<Upload> uploads = uploadService.getUserSharedUploads(page, size);
            List<UploadDto> uploadDtos = uploads.stream().map(Upload::toUploadDto).toList();
            return ResponseEntity.ok(uploadDtos);
        } catch (NotFoundException e) {
            return ResponseEntity.status(e.STATUS).build();
        }
    }

    @PostMapping("uploads/share/{id}")
    public ResponseEntity shareUploadToUser(
            @PathVariable int id,
            @RequestParam int userId
    ) {
        try {
            uploadService.shareUploadToUser(id, userId);
            return ResponseEntity.ok().build();
        } catch (HttpErrorException e) {
            return ResponseEntity.status(e.STATUS).body(e.getMessage());
        }
    }

    @DeleteMapping("uploads/share/{id}")
    public ResponseEntity removeUserFromUpload(
            @PathVariable int id,
            @RequestParam int userId
    ) {
        try {
            uploadService.removeUserFromUpload(id, userId);
            return ResponseEntity.ok().build();
        } catch (HttpErrorException e) {
            return ResponseEntity.status(e.STATUS).body(e.getMessage());
        }
    }

    @GetMapping("social")
    public ResponseEntity<List<SocialLink>> getSocialLinks() {
        try {
            List<SocialLink> socialLinks = userService.getSocialLinks();
            return ResponseEntity.ok(socialLinks);
        } catch (HttpErrorException e) {
            return ResponseEntity.status(e.STATUS).build();
        }
    }

    @PostMapping("social")
    public ResponseEntity<SocialLinkDto> addSocialLink(@RequestBody SocialLinkDto socialLinkDto) {
        try {
            SocialLink socialLink = userService.addSocialLink(socialLinkDto);
            return ResponseEntity.ok(socialLink.toSocialLinkDto());
        } catch (HttpErrorException e) {
            return ResponseEntity.status(e.STATUS).build();
        }
    }

    @DeleteMapping("social/{id}")
    public ResponseEntity removeSocialLink(@PathVariable int id) {
        try {
            userService.removeSocialLink(id);
            return ResponseEntity.ok().build();
        } catch (HttpErrorException e) {
            return ResponseEntity.status(e.STATUS).build();
        }
    }

}
