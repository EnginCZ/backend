package com.gtomy.securebin.services;

import com.gtomy.securebin.exceptions.ForbiddenException;
import com.gtomy.securebin.exceptions.NotFoundException;
import com.gtomy.securebin.models.database.Comment;
import com.gtomy.securebin.models.database.SecureBinUser;
import com.gtomy.securebin.models.database.Upload;
import com.gtomy.securebin.repositories.CommentRepository;
import com.gtomy.securebin.repositories.UploadRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CommentService {

    @Autowired
    private UserService userService;

    @Autowired
    private UploadRepository uploadRepository;
    @Autowired
    private CommentRepository commentRepository;

    public CommentService(UserService userService, UploadRepository uploadRepository, CommentRepository commentRepository) {
        this.userService = userService;
        this.uploadRepository = uploadRepository;
        this.commentRepository = commentRepository;
    }

    public int getCountOfCommentsUnderUpload(int uploadId) {
        Optional<Upload> upload = uploadRepository.findById(uploadId);
        if (upload.isEmpty()) {
            throw new NotFoundException("Upload not found");
        }
        return commentRepository.countAllByUpload(upload.get());
    }

    public List<Comment> getCommentsUnderUpload(int uploadId, int page, int size) {
        Optional<Upload> upload = uploadRepository.findById(uploadId);
        if (upload.isEmpty()) {
            throw new NotFoundException("Upload not found");
        }
        Pageable pageable = PageRequest.of(page, size, Sort.by("postedAt").descending());
        return commentRepository.findAllByUpload(upload.get(), pageable);
    }

    public Comment postComment(int uploadId, String text) {
        Optional<SecureBinUser> user = userService.getCurrentUser();
        Optional<Upload> upload = uploadRepository.findById(uploadId);
        if (user.isEmpty()) {
            throw new ForbiddenException("You have to be authorized to post comments");
        }
        if (upload.isEmpty()) {
            throw new NotFoundException("Upload not found");
        }
        Comment comment = new Comment(user.get(), upload.get(), text);
        commentRepository.save(comment);
        return comment;
    }

    public void deleteComment(int commentId) {
        Optional<SecureBinUser> user = userService.getCurrentUser();
        Optional<Comment> comment = commentRepository.findById(commentId);
        if (comment.isEmpty()) {
            throw new NotFoundException("Upload not found");
        }
        if (user.isEmpty() || comment.get().getOwner().getId() != user.get().getId()) {
            throw new ForbiddenException();
        }
        commentRepository.delete(comment.get());
    }
}
