package com.gtomy.securebin.services;

import com.gtomy.securebin.repositories.UserRepository;
import com.gtomy.securebin.models.auth.SecureBinUserDetails;
import com.gtomy.securebin.models.database.SecureBinUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class SecureBinUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<SecureBinUser> user = userRepository.findByUsername(username);
        if (user.isEmpty()) {
            throw new UsernameNotFoundException("Username " + username + " unknown.");
        }
        return new SecureBinUserDetails(user.get().getUsername(), user.get().getHashedPassword());
    }
}
