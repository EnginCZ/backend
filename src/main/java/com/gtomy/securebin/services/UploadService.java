package com.gtomy.securebin.services;

import com.gtomy.securebin.exceptions.BadRequestException;
import com.gtomy.securebin.models.dtos.CountOfUploads;
import com.gtomy.securebin.repositories.UploadRepository;
import com.gtomy.securebin.exceptions.NotFoundException;
import com.gtomy.securebin.exceptions.ForbiddenException;
import com.gtomy.securebin.models.UploadPermission;
import com.gtomy.securebin.models.database.SecureBinUser;
import com.gtomy.securebin.models.database.Upload;
import com.gtomy.securebin.models.rest.CreateUpload;
import com.gtomy.securebin.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UploadService {

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UploadRepository uploadRepository;

    public UploadService(UserService userService, UserRepository userRepository, UploadRepository uploadRepository) {
        this.userService = userService;
        this.userRepository = userRepository;
        this.uploadRepository = uploadRepository;
    }

    public Upload createUpload(CreateUpload createUpload) {
        Upload upload = new Upload(
                createUpload.displayName(),
                createUpload.uploadPermission(),
                createUpload.encryptionData()
        );
        Optional<SecureBinUser> user = userService.getCurrentUser();
        user.ifPresent(upload::setOwner);
        uploadRepository.save(upload);
        return upload;
    }

    public Upload getUpload(int id) {
        Optional<Upload> upload = uploadRepository.findById(id);
        if (upload.isEmpty()) {
            throw new NotFoundException("Upload not found");
        }
        if (!upload.get().getUploadPermission().equals(UploadPermission.PRIVATE)) {
            return upload.get();
        }
        Optional<SecureBinUser> user = userService.getCurrentUser();
        if (user.isPresent() && (upload.get().getOwner().getId() == user.get().getId() || upload.get().getAllowedUsers().contains(user.get()))) {
            return upload.get();
        }
        throw new ForbiddenException();
    }

    public CountOfUploads getUploadsCount() {
        Optional<SecureBinUser> user = userService.getCurrentUser();
        if (user.isEmpty()) {
            throw new ForbiddenException();
        }
        return new CountOfUploads(uploadRepository.countAllByOwner(user.get()), uploadRepository.countAllByAllowedUsersContaining(user.get()));
    }

    public List<Upload> getPublicUploads(int page, int size) {
        Pageable pageable = PageRequest.of(page, size, Sort.by("createdAt").descending());
        return uploadRepository.findAllByUploadPermission(UploadPermission.PUBLIC, pageable);
    }

    public List<Upload> getUserUploads(int page, int size) {
        Optional<SecureBinUser> user = userService.getCurrentUser();
        if (user.isEmpty()) {
            throw new ForbiddenException();
        }
        Pageable pageable = PageRequest.of(page, size, Sort.by("createdAt").descending());
        return uploadRepository.findAllByOwner(user.get(), pageable);
    }

    public List<Upload> getUserSharedUploads(int page, int size) {
        Optional<SecureBinUser> user = userService.getCurrentUser();
        if (user.isEmpty()) {
            throw new ForbiddenException();
        }
        Pageable pageable = PageRequest.of(page, size, Sort.by("createdAt").descending());
        return uploadRepository.findAllByAllowedUsersContaining(user.get(), pageable);
    }

    public List<SecureBinUser> getUploadAllowedUsers(int uploadId, int page, int size) {
        Optional<SecureBinUser> currentUser = userService.getCurrentUser();
        Optional<Upload> upload = uploadRepository.findById(uploadId);
        if (currentUser.isEmpty()) {
            throw new ForbiddenException();
        }
        if (upload.isEmpty()) {
            throw new NotFoundException("Upload not found");
        }
        Pageable pageable = PageRequest.of(page, size);
        return userRepository.findAllBySharedUploadsContaining(upload.get(), pageable);
    }

    public void shareUploadToUser(int uploadId, int userId) {
        Optional<SecureBinUser> currentUser = userService.getCurrentUser();
        Optional<SecureBinUser> userToShare = userRepository.findById(userId);
        Optional<Upload> upload = uploadRepository.findById(uploadId);
        if (currentUser.isEmpty()) {
            throw new ForbiddenException();
        }
        if (userToShare.isEmpty() || upload.isEmpty()) {
            throw new NotFoundException("User or Upload not found");
        }
        if (currentUser.get().getId() == userToShare.get().getId()) {
            throw new BadRequestException("You cannot share upload to yourself");
        }
        if (!upload.get().getUploadPermission().equals(UploadPermission.PRIVATE)) {
            throw new BadRequestException("You can only share private uploads");
        }
        if (upload.get().getOwner().getId() != currentUser.get().getId()) {
            throw new ForbiddenException("You can only edit your uploads");
        }
        upload.get().addAllowedUser(userToShare.get());
        uploadRepository.save(upload.get());
    }

    public void removeUserFromUpload(int uploadId, int userId) {
        Optional<SecureBinUser> currentUser = userService.getCurrentUser();
        Optional<SecureBinUser> userToShare = userRepository.findById(userId);
        Optional<Upload> upload = uploadRepository.findById(uploadId);
        if (currentUser.isEmpty()) {
            throw new ForbiddenException();
        }
        if (userToShare.isEmpty() || upload.isEmpty()) {
            throw new NotFoundException("User or Upload not found");
        }
        if (upload.get().getOwner().getId() != currentUser.get().getId()) {
            throw new ForbiddenException("You can only edit your uploads");
        }
        boolean removed = upload.get().removeAllowedUser(userToShare.get());
        if (!removed) {
            throw new NotFoundException("User not present in allowed users");
        }
        uploadRepository.save(upload.get());
    }
}
