package com.gtomy.securebin.services;

import com.gtomy.securebin.exceptions.BadRequestException;
import com.gtomy.securebin.exceptions.NotFoundException;
import com.gtomy.securebin.models.database.SocialLink;
import com.gtomy.securebin.models.dtos.SocialLinkDto;
import com.gtomy.securebin.repositories.SocialLinkRepository;
import com.gtomy.securebin.repositories.UserRepository;
import com.gtomy.securebin.models.database.SecureBinUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private SocialLinkRepository socialLinkRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    public String getCurrentUserName() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth == null || auth.getName().equals("anonymousUser")) {
            return null;
        }
        return auth.getName();
    }

    public Optional<SecureBinUser> getCurrentUser() {
        String username = getCurrentUserName();
        if (username == null) {
            return Optional.empty();
        }
        return getUserByUsername(username);
    }

    public Optional<SecureBinUser> getUserByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    public void registerUser(String username, String email, String password) {
        Optional<SecureBinUser> usernameExists = userRepository.findByUsername(username);
        Optional<SecureBinUser> emailExists = userRepository.findByEmail(email);
        if (usernameExists.isPresent()) {
            throw new BadRequestException("Username already in use");
        }
        if (emailExists.isPresent()) {
            throw new BadRequestException("Email already in use");
        }
        SecureBinUser secureBinUser = new SecureBinUser(
                username,
                email,
                passwordEncoder.encode(password)
        );
        userRepository.save(secureBinUser);
    }

    public List<SocialLink> getSocialLinks() {
        Optional<SecureBinUser> user = getCurrentUser();
        if (user.isEmpty()) {
            throw new NotFoundException("User not found");
        }
        return user.get().getSocialLinks();
    }

    public SocialLink addSocialLink(SocialLinkDto socialLinkDto) {
        Optional<SecureBinUser> user = getCurrentUser();
        if (user.isEmpty()) {
            throw new NotFoundException("User not found");
        }
        SocialLink socialLink = new SocialLink(user.get(), socialLinkDto.socialPlatform(), socialLinkDto.link());
        socialLinkRepository.save(socialLink);
        return socialLink;
    }

    public void removeSocialLink(int id) {
        Optional<SecureBinUser> user = getCurrentUser();
        if (user.isEmpty()) {
            throw new NotFoundException("User not found");
        }
        socialLinkRepository.deleteById(id);
    }
}
