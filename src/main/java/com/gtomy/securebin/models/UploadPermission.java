package com.gtomy.securebin.models;

public enum UploadPermission {
    PUBLIC, UNLISTED, PRIVATE
}
