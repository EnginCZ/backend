package com.gtomy.securebin.models.dtos;

import com.gtomy.securebin.models.EncryptionData;
import com.gtomy.securebin.models.UploadPermission;

import java.time.LocalDateTime;

public record UploadDto(
        int id,
        Integer ownerId,
        String displayName,
        UploadPermission uploadPermission,
        LocalDateTime createdAt,
        EncryptionData encryptionData
) {
}
