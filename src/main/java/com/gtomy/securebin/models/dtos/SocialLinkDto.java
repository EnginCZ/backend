package com.gtomy.securebin.models.dtos;

import com.gtomy.securebin.models.database.SocialPlatform;

public record SocialLinkDto(
        SocialPlatform socialPlatform,
        String link
) {
}
