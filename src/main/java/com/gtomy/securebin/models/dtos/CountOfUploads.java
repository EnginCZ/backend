package com.gtomy.securebin.models.dtos;

public record CountOfUploads(
        int ownedUploadsCount,
        int sharedUploadsCount
) {
}
