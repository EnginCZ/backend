package com.gtomy.securebin.models.dtos;

import java.time.LocalDateTime;
import java.util.List;

public record SecureBinUserDto(
        int id,
        String username,
        String email,
        LocalDateTime registeredAt,
        List<SocialLinkDto> socialLinks
) {}
