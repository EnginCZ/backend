package com.gtomy.securebin.models.dtos;

import java.time.LocalDateTime;

public record CommentDto(
        int id,
        int ownerId,
        String ownerName,
        String text,
        LocalDateTime postedAt
) {}
