package com.gtomy.securebin.models;

public record EncryptionData(String cipherText, String salt) {
}
