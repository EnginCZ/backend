package com.gtomy.securebin.models.database;

public enum SocialPlatform {
    YOUTUBE, TWITTER, FACEBOOK, GITHUB, GITLAB
}
