package com.gtomy.securebin.models.database;

import com.gtomy.securebin.models.dtos.SecureBinUserDto;
import jakarta.persistence.*;

import java.time.LocalDateTime;
import java.util.List;

@Entity
public class SecureBinUser {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String username;
    private String hashedPassword;

    private String email;
    private LocalDateTime registeredAt;

    @OneToMany(mappedBy="owner")
    private List<Upload> uploads;

    @OneToMany(mappedBy="owner")
    private List<Comment> comments;

    @OneToMany(mappedBy="user")
    private List<SocialLink> socialLinks;

    @ManyToMany(mappedBy = "allowedUsers")
    List<Upload> sharedUploads;

    public SecureBinUser() {
    }

    /**
     * Constructor for tests, id is automatically generated in mysql otherwise
     */
    public SecureBinUser(Integer id, String username) {
        this.id = id;
        this.username = username;
    }

    public SecureBinUser(String username, String email, String hashedPassword) {
        this.username = username;
        this.hashedPassword = hashedPassword;
        this.email = email;

        this.registeredAt = LocalDateTime.now();
    }

    public int getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getHashedPassword() {
        return hashedPassword;
    }

    public List<SocialLink> getSocialLinks() {
        return socialLinks;
    }

    public SecureBinUserDto toSecureBinUserDto() {
        return new SecureBinUserDto(
                id,
                username,
                email,
                registeredAt,
                socialLinks.stream().map(SocialLink::toSocialLinkDto).toList()
        );
    }
}
