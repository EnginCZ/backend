package com.gtomy.securebin.models.database;

import com.gtomy.securebin.models.dtos.CommentDto;
import jakarta.persistence.*;

import java.time.LocalDateTime;

@Entity
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @ManyToOne
    @JoinColumn(name="owner_id", nullable=false)
    private SecureBinUser owner;
    @ManyToOne
    @JoinColumn(name="upload_id", nullable=false)
    private Upload upload;

    @Column(columnDefinition = "TEXT")
    private String text;
    private LocalDateTime postedAt;

    public Comment() {
    }

    public Comment(SecureBinUser owner, Upload upload, String text) {
        this.owner = owner;
        this.upload = upload;
        this.text = text;
        this.postedAt = LocalDateTime.now();
    }

    public SecureBinUser getOwner() {
        return owner;
    }

    public Upload getUpload() {
        return upload;
    }

    public CommentDto toCommentDto() {
        return new CommentDto(id, owner.getId(), owner.getUsername(), text, postedAt);
    }
}
