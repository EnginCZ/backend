package com.gtomy.securebin.models.database;

import com.gtomy.securebin.models.EncryptionData;
import com.gtomy.securebin.models.UploadPermission;
import com.gtomy.securebin.models.dtos.UploadDto;
import jakarta.persistence.*;

import java.time.LocalDateTime;
import java.util.List;

@Entity
public class Upload {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @ManyToOne
    @JoinColumn(name="owner_id")
    private SecureBinUser owner;

    @OneToMany(mappedBy="upload")
    private List<Comment> comments;

    @ManyToMany
    @JoinTable(
            name = "users_uploads",
            joinColumns = @JoinColumn(name = "upload_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id")
    )
    List<SecureBinUser> allowedUsers;
    private UploadPermission uploadPermission;

    private String displayName;
    private LocalDateTime createdAt;

    @Column(columnDefinition = "TEXT")
    String cipherText;
    String salt;

    public Upload() {
    }

    /**
     * Constructor for tests, id is automatically generated in mysql otherwise
     */
    public Upload(int id, SecureBinUser owner, String displayName, UploadPermission uploadPermission, EncryptionData encryptionData) {
        this(displayName, uploadPermission, encryptionData);
        this.id = id;
        this.owner = owner;
    }

    public Upload(String displayName, UploadPermission uploadPermission, EncryptionData encryptionData) {
        this.displayName = displayName;
        this.uploadPermission = uploadPermission;

        this.cipherText = encryptionData.cipherText();
        this.salt = encryptionData.salt();

        this.owner = null;
        this.createdAt = LocalDateTime.now();
    }

    public SecureBinUser getOwner() {
        return owner;
    }

    public void setOwner(SecureBinUser owner) {
        this.owner = owner;
    }

    public UploadPermission getUploadPermission() {
        return uploadPermission;
    }

    public List<SecureBinUser> getAllowedUsers() {
        return allowedUsers;
    }

    public void addAllowedUser(SecureBinUser user) {
        allowedUsers.add(user);
    }

    public boolean removeAllowedUser(SecureBinUser user) {
        return allowedUsers.remove(user);
    }

    public UploadDto toUploadDto() {
        return new UploadDto(
                id,
                owner == null ? null : owner.getId(),
                displayName,
                uploadPermission,
                createdAt,
                new EncryptionData(cipherText, salt)
        );
    }
}
