package com.gtomy.securebin.models.database;

import com.gtomy.securebin.models.dtos.SocialLinkDto;
import jakarta.persistence.*;

@Entity
public class SocialLink {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @ManyToOne
    @JoinColumn(name="user_id", nullable=false)
    private SecureBinUser user;

    private SocialPlatform socialPlatform;
    private String link;

    public SocialLink() {
    }

    public SocialLink(SecureBinUser user, SocialPlatform socialPlatform, String link) {
        this.user = user;
        this.socialPlatform = socialPlatform;
        this.link = link;
    }

    public int getId() {
        return id;
    }

    public SocialPlatform getSocialPlatform() {
        return socialPlatform;
    }

    public String getLink() {
        return link;
    }

    public SocialLinkDto toSocialLinkDto() {
        return new SocialLinkDto(socialPlatform, link);
    }
}
