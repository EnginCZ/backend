package com.gtomy.securebin.models.rest;

public record UserCredentials(String username, String password) {
}
