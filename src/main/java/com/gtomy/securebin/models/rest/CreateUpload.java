package com.gtomy.securebin.models.rest;

import com.gtomy.securebin.models.EncryptionData;
import com.gtomy.securebin.models.UploadPermission;

public record CreateUpload(
        String displayName,
        UploadPermission uploadPermission,
        EncryptionData encryptionData
) {
}
