package com.gtomy.securebin.models.rest;

public record RegisterUser (
        String username,
        String email,
        String password
){}
