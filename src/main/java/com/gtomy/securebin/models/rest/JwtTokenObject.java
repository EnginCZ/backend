package com.gtomy.securebin.models.rest;

public record JwtTokenObject(String accessToken) {
}
