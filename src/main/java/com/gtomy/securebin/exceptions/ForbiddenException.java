package com.gtomy.securebin.exceptions;

public class ForbiddenException extends HttpErrorException {
    public ForbiddenException() {
        super("You dont have permission to view this page", 403);
    }

    public ForbiddenException(String message) {
        super(message, 403);
    }
}
