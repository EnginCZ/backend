package com.gtomy.securebin.exceptions;

public class NotFoundException extends HttpErrorException {
    public NotFoundException(){
        super("Item not found", 404);
    }

    public NotFoundException(String message) {
        super(message, 404);
    }
}
