package com.gtomy.securebin.exceptions;

public class BadRequestException extends HttpErrorException {
    public BadRequestException() {
        super("Bad request", 400);
    }

    public BadRequestException(String message) {
        super(message, 400);
    }

}
