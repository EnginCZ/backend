package com.gtomy.securebin.exceptions;

import org.springframework.http.HttpStatusCode;

public class HttpErrorException extends RuntimeException {
    public final HttpStatusCode STATUS;

    public HttpErrorException(String message, int status) {
        super(message);
        this.STATUS = HttpStatusCode.valueOf(status);
    }

}
