package com.gtomy.securebin.utils;

import com.gtomy.securebin.models.auth.SecureBinUserDetails;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.PrematureJwtException;
import io.jsonwebtoken.security.SignatureException;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import java.io.IOException;

@Component
public class JwtRequestFilter extends OncePerRequestFilter {

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        final String requestTokenHeader = request.getHeader("Authorization");

        if (requestTokenHeader != null && requestTokenHeader.startsWith("Bearer ")) {
            String jwtToken = requestTokenHeader.substring(7);

            boolean isJwtTokenValid = false;
            try {
                isJwtTokenValid = jwtTokenUtil.isTokenValid(jwtToken);
            } catch (SignatureException ex) {
                logger.info("JWT token signature issue");
            } catch (MalformedJwtException ex){
                logger.info("JWT token invalid format");
            } catch (PrematureJwtException ex){
                logger.info("JWT token accessed before its validity.");
            } catch (ExpiredJwtException ex){
                logger.info("JWT token expired.");
            }

            if (isJwtTokenValid) {
                String userName = jwtTokenUtil.getClaimsFromToken(jwtToken).getSubject();
                UserDetails userDetails = new SecureBinUserDetails(userName, null);
                UsernamePasswordAuthenticationToken upat = new UsernamePasswordAuthenticationToken(
                        userDetails, null, userDetails.getAuthorities());
                upat.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                SecurityContextHolder.getContext().setAuthentication(upat);
            } else {
                logger.info("JWT token invalid.");
            }
        } else {
            logger.warn("JWT Token does not begin with Bearer String");
        }
        filterChain.doFilter(request, response);
    }
}
