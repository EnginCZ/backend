package com.gtomy.securebin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SecurebinApplication {

	public static void main(String[] args) {
		SpringApplication.run(SecurebinApplication.class, args);
	}

}
