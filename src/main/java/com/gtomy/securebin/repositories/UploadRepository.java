package com.gtomy.securebin.repositories;

import com.gtomy.securebin.models.UploadPermission;
import com.gtomy.securebin.models.database.SecureBinUser;
import com.gtomy.securebin.models.database.Upload;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UploadRepository extends JpaRepository<Upload, Integer> {
    List<Upload> findAllByUploadPermission(UploadPermission uploadPermission, Pageable pageable);

    List<Upload> findAllByOwner(SecureBinUser owner, Pageable pageable);

    List<Upload> findAllByAllowedUsersContaining(SecureBinUser user, Pageable pageable);


    int countAllByAllowedUsersContaining(SecureBinUser user);
    int countAllByOwner(SecureBinUser owner);
}
