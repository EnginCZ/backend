package com.gtomy.securebin.repositories;

import com.gtomy.securebin.models.database.Comment;
import com.gtomy.securebin.models.database.Upload;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CommentRepository extends JpaRepository<Comment, Integer> {

    List<Comment> findAllByUpload(Upload upload, Pageable pageable);

    int countAllByUpload(Upload upload);

}
