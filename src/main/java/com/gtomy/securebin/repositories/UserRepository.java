package com.gtomy.securebin.repositories;

import com.gtomy.securebin.models.database.SecureBinUser;
import com.gtomy.securebin.models.database.Upload;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends CrudRepository<SecureBinUser, Integer> {
    Optional<SecureBinUser> findByUsername(String username);
    Optional<SecureBinUser> findByEmail(String email);

    List<SecureBinUser> findAllBySharedUploadsContaining(Upload upload, Pageable pageable);
}
