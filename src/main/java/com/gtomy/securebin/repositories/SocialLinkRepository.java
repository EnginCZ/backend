package com.gtomy.securebin.repositories;

import com.gtomy.securebin.models.database.SocialLink;
import org.springframework.data.repository.CrudRepository;

public interface SocialLinkRepository extends CrudRepository<SocialLink, Integer> {
}
