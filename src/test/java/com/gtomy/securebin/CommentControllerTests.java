package com.gtomy.securebin;

import com.gtomy.securebin.controllers.CommentController;
import com.gtomy.securebin.models.EncryptionData;
import com.gtomy.securebin.models.UploadPermission;
import com.gtomy.securebin.models.database.Comment;
import com.gtomy.securebin.models.database.SecureBinUser;
import com.gtomy.securebin.models.database.Upload;
import com.gtomy.securebin.models.dtos.CommentDto;
import com.gtomy.securebin.repositories.CommentRepository;
import com.gtomy.securebin.repositories.UploadRepository;
import com.gtomy.securebin.services.CommentService;
import com.gtomy.securebin.services.UserService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.any;
import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
public class CommentControllerTests {

    private static final UserService userService = Mockito.mock(UserService.class);
    private static final UploadRepository uploadRepository = Mockito.mock(UploadRepository.class);
    private static final CommentRepository commentRepository = Mockito.mock(CommentRepository.class);

    private static CommentController commentController;

    @BeforeAll
    static void setup() {
        Upload upload = new Upload("Test upload", UploadPermission.PUBLIC, new EncryptionData("CIPHER_TEXT", "JUST_SALT_FOR_TESTING"));
        SecureBinUser testUser = new SecureBinUser(0, "testuser1");

        when(userService.getCurrentUser()).thenReturn(Optional.of(testUser));

        when(commentRepository.findById(any(int.class))).thenReturn(Optional.of(new Comment(testUser, upload, "Test comment 1")));
        when(commentRepository.findAllByUpload(any(Upload.class), any(Pageable.class))).thenReturn(List.of(
                new Comment(testUser, upload, "Test comment 1"),
                new Comment(testUser, upload, "Test comment 2"),
                new Comment(testUser, upload, "Test comment 3"),
                new Comment(testUser, upload, "Test comment 4")
        ));

        when(uploadRepository.findById(any(int.class))).thenReturn(Optional.of(upload));

        CommentService commentService = new CommentService(userService, uploadRepository, commentRepository);
        commentController = new CommentController(commentService);
    }

    @Test
    void getCommentsUnderUpload() {
        ResponseEntity<List<CommentDto>> commentDtos = commentController.getCommentsOnUpload(0, 0, 10);

        assertEquals(HttpStatusCode.valueOf(200), commentDtos.getStatusCode());
        assertNotNull(commentDtos);
        assertEquals(4, commentDtos.getBody().size());
    }

    @Test
    void postCommentSuccess() {
        ResponseEntity<CommentDto> commentDto = commentController.postComment(0, "Super long text to post");

        assertEquals(HttpStatusCode.valueOf(200), commentDto.getStatusCode());
        assertNotNull(commentDto.getBody());
        assertEquals(0, commentDto.getBody().ownerId());
        assertEquals("Super long text to post", commentDto.getBody().text());
    }

    @Test
    void postCommentShortText() {
        ResponseEntity<CommentDto> commentDto = commentController.postComment(0, "ShortText");
        assertEquals(HttpStatusCode.valueOf(400), commentDto.getStatusCode());
    }

    @Test
    void deleteComment() {
        ResponseEntity commentDelete = commentController.deleteComment(0);
        assertEquals(HttpStatusCode.valueOf(200), commentDelete.getStatusCode());
    }
}
