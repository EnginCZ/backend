package com.gtomy.securebin;

import com.gtomy.securebin.controllers.UploadController;
import com.gtomy.securebin.models.EncryptionData;
import com.gtomy.securebin.models.UploadPermission;
import com.gtomy.securebin.models.database.SecureBinUser;
import com.gtomy.securebin.models.database.Upload;
import com.gtomy.securebin.models.dtos.UploadDto;
import com.gtomy.securebin.models.rest.CreateUpload;
import com.gtomy.securebin.repositories.UploadRepository;
import com.gtomy.securebin.services.UploadService;
import com.gtomy.securebin.services.UserService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class UploadControllerTests {

    private static final UserService userService = Mockito.mock(UserService.class);
    private static final UploadRepository uploadRepository = Mockito.mock(UploadRepository.class);

    private static UploadController uploadController;

    @BeforeAll
    static void setup() {
        SecureBinUser user = new SecureBinUser(0, "testuser1");
        when(userService.getCurrentUser()).thenReturn(Optional.of(user));
        when(uploadRepository.save(any(Upload.class))).thenReturn(new Upload("Test upload", UploadPermission.PUBLIC, new EncryptionData("CIPHER_TEXT", "JUST_SALT_FOR_TESTING")));

        when(uploadRepository.findAllByUploadPermission(any(UploadPermission.class), any(Pageable.class))).thenReturn(List.of(
                new Upload(1, user,"Test upload", UploadPermission.PUBLIC, new EncryptionData("CIPHER_TEXT", "JUST_SALT_FOR_TESTING")),
                new Upload(2, user,"Test upload 2", UploadPermission.PUBLIC, new EncryptionData("CIPHER_TEXT", "JUST_SALT_FOR_TESTING")),
                new Upload(3, user,"Test upload 3", UploadPermission.PUBLIC, new EncryptionData("CIPHER_TEXT", "JUST_SALT_FOR_TESTING"))
        ));

        uploadController = new UploadController(new UploadService(userService, null, uploadRepository));
    }

    @Test
    void getListOfPublicUploads() {
        ResponseEntity<List<UploadDto>> uploadDtos = uploadController.getPublicUploads(0, 10);

        assertEquals(HttpStatusCode.valueOf(200), uploadDtos.getStatusCode());
        assertNotNull(uploadDtos.getBody());
        assertEquals(3, uploadDtos.getBody().size());
    }

    @Test
    void createUpload() {
        CreateUpload createUpload = new CreateUpload("Test upload", UploadPermission.PUBLIC, new EncryptionData("CIPHER_TEXT", "JUST_SALT_FOR_TESTING"));
        ResponseEntity<UploadDto> upload = uploadController.createUpload(createUpload);

        assertEquals(HttpStatusCode.valueOf(201), upload.getStatusCode());
        assertNotNull(upload.getBody());
        assertEquals("Test upload", upload.getBody().displayName());
        assertEquals(new EncryptionData("CIPHER_TEXT", "JUST_SALT_FOR_TESTING") ,upload.getBody().encryptionData());
    }

    @Test
    void getUploadSuccess() {
        Upload upload = new Upload("Test upload", UploadPermission.PUBLIC, new EncryptionData("CIPHER_TEXT", "JUST_SALT_FOR_TESTING"));
        when(uploadRepository.findById(any(int.class))).thenReturn(Optional.of(upload));

        ResponseEntity<UploadDto> uploadDto = uploadController.getUpload(0);

        assertEquals(HttpStatusCode.valueOf(200), uploadDto.getStatusCode());
        assertNotNull(uploadDto.getBody());
    }

    @Test
    void getUploadUnauthorized() {
        when(userService.getCurrentUser()).thenReturn(Optional.empty());

        Upload privateUpload = new Upload("Test upload", UploadPermission.PRIVATE, new EncryptionData("CIPHER_TEXT", "JUST_SALT_FOR_TESTING"));
        when(uploadRepository.findById(any(int.class))).thenReturn(Optional.of(privateUpload));

        assertEquals(ResponseEntity.status(HttpStatusCode.valueOf(403)).build(), uploadController.getUpload(0));
    }
}
