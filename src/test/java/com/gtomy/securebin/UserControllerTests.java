package com.gtomy.securebin;

import com.gtomy.securebin.controllers.UserController;
import com.gtomy.securebin.models.EncryptionData;
import com.gtomy.securebin.models.UploadPermission;
import com.gtomy.securebin.models.database.SecureBinUser;
import com.gtomy.securebin.models.database.Upload;
import com.gtomy.securebin.models.dtos.UploadDto;
import com.gtomy.securebin.repositories.UploadRepository;
import com.gtomy.securebin.repositories.UserRepository;
import com.gtomy.securebin.services.UploadService;
import com.gtomy.securebin.services.UserService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class UserControllerTests {

    private static final UserService userService = Mockito.mock(UserService.class);
    private static final UploadRepository uploadRepository = Mockito.mock(UploadRepository.class);
    private static final UserRepository userRepository = Mockito.mock(UserRepository.class);

    private static UserController userController;

    @BeforeAll
    static void setup() {
        SecureBinUser user = new SecureBinUser(0, "testuser1");
        when(userService.getCurrentUser()).thenReturn(Optional.of(user));

        when(uploadRepository.findAllByOwner(any(SecureBinUser.class), any(Pageable.class))).thenReturn(List.of(
                new Upload(1, user, "Test upload", UploadPermission.PUBLIC, new EncryptionData("CIPHER_TEXT", "JUST_SALT_FOR_TESTING")),
                new Upload(2, user,"Test upload 2", UploadPermission.PUBLIC, new EncryptionData("CIPHER_TEXT", "JUST_SALT_FOR_TESTING")),
                new Upload(3, user,"Test upload 3", UploadPermission.PUBLIC, new EncryptionData("CIPHER_TEXT", "JUST_SALT_FOR_TESTING"))
        ));

        when(uploadRepository.findAllByAllowedUsersContaining(any(SecureBinUser.class), any(Pageable.class))).thenReturn(List.of(
                new Upload(1, user,"Test upload", UploadPermission.PUBLIC, new EncryptionData("CIPHER_TEXT", "JUST_SALT_FOR_TESTING")),
                new Upload(2, user,"Test upload 2", UploadPermission.PUBLIC, new EncryptionData("CIPHER_TEXT", "JUST_SALT_FOR_TESTING")),
                new Upload(3, user,"Test upload 3", UploadPermission.PUBLIC, new EncryptionData("CIPHER_TEXT", "JUST_SALT_FOR_TESTING")),
                new Upload(4, user,"Test upload 4", UploadPermission.PUBLIC, new EncryptionData("CIPHER_TEXT", "JUST_SALT_FOR_TESTING")),
                new Upload(5, user,"Test upload 5", UploadPermission.PUBLIC, new EncryptionData("CIPHER_TEXT", "JUST_SALT_FOR_TESTING"))
        ));

        when(userRepository.findById(any(int.class))).thenReturn(Optional.of(new SecureBinUser(1, "testuser1")));

        UploadService uploadService = new UploadService(userService, userRepository, uploadRepository);
        userController = new UserController(userService, uploadService);
    }

    @Test
    void getListOfOwnedUploads() {
        ResponseEntity<List<UploadDto>> uploadDtos = userController.getUserUploads(0, 10);

        assertEquals(HttpStatusCode.valueOf(200), uploadDtos.getStatusCode());
        assertNotNull(uploadDtos.getBody());
        assertEquals(3, uploadDtos.getBody().size());
    }

    @Test
    void getListOfSharedUploads() {
        ResponseEntity<List<UploadDto>> uploadDtos = userController.getUserSharedUploads(0, 10);

        assertEquals(HttpStatusCode.valueOf(200), uploadDtos.getStatusCode());
        assertNotNull(uploadDtos.getBody());
        assertEquals(5, uploadDtos.getBody().size());
    }

    @Test
    void shareUploadToUserAuthFail() {
        Upload upload = new Upload("Test upload", UploadPermission.PRIVATE, new EncryptionData("CIPHER_TEXT", "JUST_SALT_FOR_TESTING"));
        upload.setOwner(new SecureBinUser(1, "testuser2"));
        when(uploadRepository.findById(any(int.class))).thenReturn(Optional.of(upload));

        ResponseEntity shared = userController.shareUploadToUser(0, 1);
        assertEquals(HttpStatusCode.valueOf(403), shared.getStatusCode());
    }

    @Test
    void shareUploadToUserPublicFail() {
        Upload upload = new Upload("Test upload", UploadPermission.PUBLIC, new EncryptionData("CIPHER_TEXT", "JUST_SALT_FOR_TESTING"));
        upload.setOwner(new SecureBinUser(0, "testuser1"));
        when(uploadRepository.findById(any(int.class))).thenReturn(Optional.of(upload));

        ResponseEntity shared = userController.shareUploadToUser(0, 1);
        assertEquals(HttpStatusCode.valueOf(400), shared.getStatusCode());
    }
}
